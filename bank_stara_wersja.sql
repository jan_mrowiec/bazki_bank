drop database System_Bankowy;

Create Database System_Bankowy;

USE System_Bankowy;

Create Table Klienci(
    Id_klienta int PRIMARY KEY IDENTITY NOT NULL,
    Imie nvarchar(20) NOT NULL,
    Nazwisko nvarchar(20) NOT NULL,
    Pesel nvarchar(11) UNIQUE NOT NULL,
    Adres nvarchar(80) NOT NULL,
    Kod_Pocztowy nvarchar(10) NOT NULL,
    Miasto nvarchar (60) NOT NULL,
    Kraj nvarchar(60) DEFAULT 'Polska' NOT NULL
)

Create Table Firmy
(
    Id_firmy int PRIMARY KEY IDENTITY NOT NULL,
    Nazwa nvarchar(60) NOT NULL,
    NIP nvarchar(10) UNIQUE NOT NULL,
    Adres nvarchar(80) NOT NULL,
    Kod_Pocztowy nvarchar(10) NOT NULL,
    Miasto nvarchar (60) NOT NULL,
    Kraj nvarchar(60) DEFAULT 'Polska' NOT NULL
)

Create Table Bankomaty
(
    Id_Bankomatu int PRIMARY KEY IDENTITY NOT NULL,
    Wplatomat bit NOT NULL,
    Adres nvarchar(80) NOT NULL,
    Kod_Pocztowy nvarchar(10) NOT NULL,
    Miasto nvarchar (60) NOT NULL,
    Kraj nvarchar(60) DEFAULT 'Polska' NOT NULL,
)

Create Table Waluty(
    Id_waluty int primary key Identity Not null,
    Nazwa nvarchar(60) NOT NULL,
	Akronim nvarchar(3)
)

Create Table Rodzaje_kont(
    Id_rodzaju_konta int PRIMARY KEY Identity NOT NULL,
    Rodzaj_Konta varchar(40) NOT NULL,
)



Create Table Konta(
    Id_konta int PRIMARY KEY Identity NOT NULL,
    Id_typu_konta int FOREIGN KEY REFERENCES Rodzaje_kont(Id_rodzaju_konta),
    Kraj nVARCHAR(10),
    Nr_konta nVARCHAR(28),
    Saldo Money,
    Id_waluty int FOREIGN KEY REFERENCES Waluty(Id_waluty),
    Otwarte bit default 1, --czy konto jest otwarte ( np lokata zamyka konto)
)

Create table Typy_kart(
    Id_typu_karty int PRIMARY KEY Identity NOT NULL,
    Nazwa varchar(40) NOT NULL,
)


Create Table Karty
(
    Nr_karty int PRIMARY KEY IDENTITY NOT NULL,
    Id_klienta int FOREIGN KEY REFERENCES Konta(Id_konta) NOT NULL,
    Id_typu_karty int FOREIGN KEY REFERENCES Typy_kart(Id_typu_karty) NOT NULL
)

Create Table Karty_Konta
(
	Nr_Karty int FOREIGN KEY REFERENCES Karty(Nr_karty) on delete cascade NOT NULL,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) on delete cascade NOT NULL,
	CONSTRAINT PK_kont PRIMARY KEY (Nr_Karty,Id_konta)
)


Create Table Transakcje
(
    Id_transakcji int PRIMARY KEY NOT NULL,
    Kwota Money,
)

Create Table Przelewy
(
    Id_przelewu int Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_nadawcy int FOREIGN KEY REFERENCES Konta(Id_konta),
    Id_odbiorcy int FOREIGN KEY REFERENCES Konta(Id_konta),
    Status_transakcji char,
    Data_zlecenia DATE Not null,
    Data_realizacji DATE Not null,
    Tytul Nvarchar(100) Not null,
)

Create Table Wplaty
(
    Id_wplaty int  Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_Bankomatu int FOREIGN KEY REFERENCES Bankomaty(Id_Bankomatu) Not null,
    Id_karty int FOREIGN KEY REFERENCES Karty(Nr_karty) Not null,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) Not null,
    Data_realizacji Date, 
)

Create Table Wyplaty
(
    Id_wyplaty int Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_Bankomatu int FOREIGN KEY REFERENCES Bankomaty(Id_Bankomatu) Not null,
    Id_karty int FOREIGN KEY REFERENCES Karty(Nr_karty) Not null,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) Not null,
    Data_realizacji Date,
)

Create Table Platnosci
(
    Id_platnosci int Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_karty int FOREIGN KEY REFERENCES Karty(Nr_karty) Not null,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) Not null,
    Data_realizacji Date,
    Tytul Nvarchar(100),
)

Create Table Lokaty
(
    Id_lokaty int Primary key Identity Not null,
    Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta),
    Procent float,
    Data_poczatku_lokaty DATE,
    Data_konca_lokaty DATE,
)

Create Table Pracownicy_firm
(
    Id_firmy int FOREIGN KEY REFERENCES Firmy(Id_firmy),
    Id_klienta int FOREIGN KEY REFERENCES Klienci(Id_klienta)
)
Create Table Wlasciciele_prywatnych_kont(
    Id_klienta int FOREIGN KEY REFERENCES Klienci(Id_klienta),
    Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta),
	CONSTRAINT PK_p PRIMARY KEY (Id_klienta,Id_konta)
)

Create Table Wlasciciele_firmowych_kont(
    Id_firmy int FOREIGN KEY REFERENCES Firmy(Id_firmy),
    Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta),
	CONSTRAINT PK_f PRIMARY KEY (Id_firmy,Id_konta)
)


--DODATKOWE FUNKCJE

--1. Saldo dla idRachunku
go
CREATE FUNCTION SaldoDlaRachunku(@IdRachunku INT)
RETURNS TABLE AS
RETURN (SELECT K.Id_konta, K.Nr_konta, K.Saldo FROM Konta AS K WHERE K.Id_konta = @IdRachunku)

--2. Czy konto jest aktywne dla karty
go
CREATE FUNCTION CzyKontoJestAktywne(@IdKonta INT)
RETURNS BIT
AS
BEGIN
RETURN (select Otwarte from Konta where Id_konta = @IdKonta)
END

go

Create FUNCTION konto_exists(@Nr_konta nvarchar(28))
RETURNS bit AS
BEGIN
IF exists( select Konta.Nr_konta from Konta where Konta.Nr_konta = @Nr_konta)
	begin
		return 1
	end
return 0 
END

go

CREATE FUNCTION Wlasciciele(@Nr_konta NVARCHAR(28))
RETURNS TABLE AS
	RETURN (select Klienci.* from Wlasciciele_prywatnych_kont inner join Klienci on Wlasciciele_prywatnych_kont.Id_Klienta = Klienci.Id_Klienta inner join Konta on Wlasciciele_prywatnych_kont.Id_konta = Konta.Id_konta where Konta.Nr_konta = @Nr_konta)
go

--PROCEDURY

--1. Wp?ata na konto
go
create PROCEDURE dbo.Wplata(@NrKarty nvarchar(16), @IdKonta int, @NrBankomatu int, @Kwota money) 
as
IF(@Kwota <=0) 
BEGIN
    RAISERROR('Wp?acana kwota musi by? wi?ksza od 0', 1, 1)
	RETURN
END
IF not exists (Select * FROM Bankomaty AS B WHERE B.Id_Bankomatu=@NrBankomatu)
BEGIN
    RAISERROR( 'Brak bankomatu o takim numerze', 1, 1)
	RETURN
END
IF not exists (Select * FROM Karty AS K WHERE K.Nr_karty=@NrKarty)
BEGIN
    RAISERROR( 'Brak takiej karty', 1, 1)
	RETURN
END
IF not exists (Select * FROM Karty_Konta AS K WHERE K.Nr_karty=@NrKarty AND K.Id_konta= @IdKonta)
BEGIN
    RAISERROR( 'Dane konto nie istnieje albo nie jest przypisane do danej kary', 1, 1)
	RETURN
END
DECLARE @IdTransakcji int
DECLARE @IdKarty int
SET @IdTransakcji = NEXT VALUE FOR ID_TRANSAKCJI
INSERT INTO Transakcje VALUES (@IdTransakcji,@Kwota)
INSERT INTO Wplaty VALUES (@IdTransakcji,@NrBankomatu, @NrKarty, @IdKonta,GETDATE())
GO






--2. Wyp?ata z konta
create PROCEDURE dbo.Wyplata(@NrKarty nvarchar(16), @Id_konta int, @NrBankomatu int, @Kwota money) 
as
IF(@Kwota <=0) 
BEGIN
    RAISERROR( 'Wyp?acana kwota musi by? wi?ksza od 0', 1 ,1)
    Return
END
IF @Kwota > (SELECT KO.Saldo FROM Konta AS KO where @Id_konta = KO.Id_konta)
BEGIN
    RAISERROR( 'Niewystarczaj?ca ilo?? ?rodkó na koncie aby dokona? wyp?aty', 1, 1 )
    Return
END
IF not exists (Select * FROM Bankomaty AS B WHERE B.Id_Bankomatu=@NrBankomatu)
BEGIN
    RAISERROR( 'Brak bankomatu o takim numerze', 1, 1)
    Return
END
IF 0 = (Select B.Wplatomat FROM Bankomaty AS B WHERE B.Id_Bankomatu=@NrBankomatu)
BEGIN
    RAISERROR( 'Podany bankomat nie jest wp?atomatem', 1, 1)
    Return
END
IF not exists (Select * FROM Karty AS K WHERE K.Nr_karty=@NrKarty)
BEGIN
    RAISERROR( 'Brak konta o takim numerze karty', 1, 1)
    Return
END

IF not exists (Select * FROM Karty_Konta AS K WHERE K.Nr_karty=@NrKarty AND K.Id_konta= @Id_konta)
BEGIN
    RAISERROR( 'Dane konto nie istnieje albo nie jest przypisane do danej kary', 1, 1)
	RETURN
END
DECLARE @IdTransakcji int
SET @IdTransakcji = NEXT VALUE FOR ID_TRANSAKCJI
INSERT INTO Transakcje VALUES (@IdTransakcji,@Kwota)
INSERT INTO Wplaty VALUES (@IdTransakcji,@NrBankomatu,@NrKarty, @Id_konta,GETDATE())
GO





--3. Natychmiastowy przelew
create PROC dbo.PrzelewNatychmiastowy(
    @IdNadawcy int,
    @IdOdbiorcy int, 
    @Kwota Money, 
    @Tytul NVARCHAR(100)
    )
AS
BEGIN TRANSACTION
if not exists (Select K.Id_konta FROM Konta as K WHERE K.Id_konta = @IdNadawcy)
BEGIN
    RAISERROR( 'Nie istnieje konto nadawcy', 1, 1)
    ROLLBACK
    return
END
if not exists (Select K.Id_konta FROM Konta as K WHERE K.Id_konta = @IdOdbiorcy)
BEGIN
    RAISERROR( 'Nie istnieje konto odbiorcy', 1, 1)
    ROLLBACK
    return
END
if @Kwota < 0
BEGIN
    RAISERROR( 'Kwota musi by? wi?ksza od 0', 1, 1)
    ROLLBACK
    return
END
if @Kwota > (Select K.Saldo FROM Konta as K Where K.Id_konta = @IdNadawcy)
BEGIN
    RAISERROR( 'Niewystarczaj?ca kwota na koncie nadawcy', 1, 1)
    ROLLBACK
    return
END
UPDATE Konta 
SET Saldo = Saldo - @Kwota
Where Id_konta = @IdNadawcy

UPDATE Konta 
SET Saldo = Saldo + @Kwota
Where Id_konta = @IdOdbiorcy

Declare @IdTransakcji INT
SET @IdTransakcji = (NEXT VALUE FOR ID_TRANSAKCJI)

INSERT INTO Transakcje Values 
(@IdTransakcji, @Kwota)

INSERT INTO Przelewy Values
(@IdTransakcji,@IdNadawcy,@IdOdbiorcy,'',GETDATE(), GETDATE(),@Tytul)

IF @@ERROR = 0
BEGIN
    PRINT 'Transakcja zako?czy?a si? powodzeniem'
    COMMIT TRANSACTION
END
ELSE
BEGIN
    RAISERROR('Nast?pi? b??d',16,34)
    ROLLBACK TRANSACTION
END
GO


Create PROCEDURE zalorz_konto_prywatne (@Nr_konta NVARCHAR(28), @Id_typu int, @Kraj nvarchar(10), @Waluta int, @Wlasciel int)
AS
IF (dbo.konto_exists(@Nr_konta) = 1)
Begin
	RAISERROR( 'To konto juz istnieje', 1, 1);
	return
END

IF not exists ( select Klienci.Id_Klienta from Klienci where Klienci.Id_Klienta = @Wlasciel)
Begin
	RAISERROR( 'Podany klient nie istnieje', 1, 1);
	return
END
	INSERT INTO Konta(Id_typu_konta, Kraj, Nr_konta, Saldo, Id_waluty, Otwarte) values (@Id_typu, @Kraj, @Nr_konta, 0, @Waluta, 1);
	declare @Id_konta int = (select Id_konta from Konta K where K.Nr_konta = @Nr_konta)
	INSERT INTO Wlasciciele_prywatnych_kont values (@Wlasciel,@Id_konta);
GO


Create PROCEDURE zalorz_firmowych_prywatne (@Nr_konta NVARCHAR(28), @Id_typu int, @Kraj nvarchar(10), @Waluta int, @Wlasciel int)
AS
IF (dbo.konto_exists(@Nr_konta) = 1)
Begin
	RAISERROR( 'To konto juz istnieje', 1, 1);
	return
END

IF not exists ( select Firmy.Id_firmy from Firmy where Id_firmy = @Wlasciel)
Begin
	RAISERROR( 'Podana firma nie istnieje', 1, 1);
	return
END
	INSERT INTO Konta(Id_typu_konta, Kraj, Nr_konta, Saldo, Id_waluty, Otwarte) values (@Id_typu, @Kraj, @Nr_konta, 0, @Waluta, 1);
	declare @Id_konta int = (select Id_konta from Konta K where K.Nr_konta = @Nr_konta)
	INSERT INTO Wlasciciele_firmowych_kont values (@Wlasciel, @Id_konta);
GO


--4. P?atno?? Kart?
/*CREATE PROC dbo.PlatnoscKarta (@NrKarty nvarchar(16), @Kwota money, @Tytul NVARCHAR(100))
AS
BEGIN TRANSACTION
if @Kwota <= 0
BEGIN
    RAISERROR( 'Kwota musi byc wieksza od 0', 1, 1)
    ROLLBACK
    RETURN
END
IF not exists (Select * FROM Karty AS K WHERE K.Nr_karty=@NrKarty)
BEGIN
    RAISERROR( 'Brak konta o takim numerze karty', 1, 1)
    ROLLBACK
    Return
END
IF @Kwota > (
SELECT KO.Saldo 
FROM KARTY AS K 
JOIN Konta AS KO 
ON KO.Id_konta=K.Id_nr_konta 
WHERE K.Nr_karty=@NrKarty) 
BEGIN
    RAISERROR( 'Nie wystarcajaca ilosc ?rodków na koncie', 1, 1)
    ROLLBACK
    Return
END
DECLARE @IdKarty INT
SET @IdKarty = (SELECT K.Nr_karty FROM Karty AS K WHERE K.Nr_karty = @NrKarty)
IF 0 = dbo.CzyKontoJestAktywneIdKarty(@IdKarty)
BEGIN
    RAISERROR( 'Podane Konto jest zamkni?te', 1, 1)
    ROLLBACK
    Return
END
DECLARE @IdTransakcji INT
SET @IdTransakcji = (NEXT VALUE FOR ID_TRANSAKCJI)

DECLARE @IdKonta INT
SET @IdKonta = (SELECT K.Id_nr_konta FROM Karty AS K WHERE K.Nr_karty = @NrKarty)

INSERT INTO Transakcje Values 
(@IdTransakcji, @Kwota)

INSERT INTO Platnosci Values
(@IdTransakcji,@IdKarty,GETDATE(),@Tytul)

UPDATE Konta
SET Saldo = Saldo - @Kwota
WHERE Id_konta = @IdKonta

IF @@ERROR = 0
BEGIN
    PRINT 'Transakcja zako?czy?a si? powodzeniem'
    COMMIT TRANSACTION
END
ELSE
BEGIN
    RAISERROR('Nast?pi? b??d',16,34)
    ROLLBACK TRANSACTION
END
GO*/





--TRIGGERY
--1. Uaktualnienie po wp?acie

CREATE TRIGGER Uaktualnij_saldo_po_wplacie ON Wplaty
AFTER INSERT
AS
DECLARE @IdTransakcji INT
DECLARE @KwotaTransakcji Money
DECLARE @IdKarty INT
DECLARE @IdKonta Int
SET @IdTransakcji = (SELECT Id_wplaty FROM inserted)
SET @IdKarty= (SELECT Id_karty FROM inserted)
SET @KwotaTransakcji = (Select T.Kwota FROM Transakcje AS T WHERE T.Id_transakcji = @IdTransakcji)
SET @IdKonta = (SELECT Id_konta FROM inserted)
UPDATE Konta 
SET Saldo = Saldo + @KwotaTransakcji
Where Id_konta = @IdKonta
GO

--2. Uaktualnienie po wyp?acie

CREATE TRIGGER Uaktualnij_saldo_po_wyplacie ON Wyplaty
AFTER INSERT
AS
DECLARE @IdTransakcji INT
DECLARE @KwotaTransakcji Money
DECLARE @IdKarty INT
DECLARE @IdKonta Int
SET @IdTransakcji = (SELECT Id_wyplaty FROM inserted)
SET @IdKarty= (SELECT Id_karty FROM inserted)
SET @KwotaTransakcji = (Select T.Kwota FROM Transakcje AS T WHERE T.Id_transakcji = @IdTransakcji)
SET @IdKonta = (SELECT Id_konta FROM inserted)
UPDATE Konta 
SET Saldo = Saldo - @KwotaTransakcji
Where Id_konta = @IdKonta
GO

Create Trigger Usun_Wlasiciela On Wlasciciele_prywatnych_kont
Instead of delete
as
	DECLARE @table_d table(Id_konta int, count int)
	insert into @table_d select Id_konta, count(*) from deleted group by Id_konta;

	DECLARE @table_a table(Id_konta int, count int)
	insert into @table_a select Id_konta, count(*) from Wlasciciele_prywatnych_kont group by Id_konta;

	if exists (select d.Id_konta from @table_d d inner join @table_a a on d.Id_Konta = a.Id_Konta inner join Konta k on k.Id_konta = A.Id_konta where d.count = a.count)
	begin

		RAISERROR('Nie możesz usunąć ostatniego właściela konta', 1, 1)
		return;
	end
	Delete Wlasciciele_prywatnych_kont from Wlasciciele_prywatnych_kont inner join deleted on Wlasciciele_prywatnych_kont.Id_klienta = deleted.Id_klienta and Wlasciciele_prywatnych_kont.Id_konta = deleted.Id_konta;
go


Create Trigger Usun_Wlasiciela_fir On Wlasciciele_firmowych_kont
Instead of delete
as
	DECLARE @table_d table(Id_konta int, count int)
	insert into @table_d select Id_konta, count(*) from deleted group by Id_konta;

	DECLARE @table_a table(Id_konta int, count int)
	insert into @table_a select Id_konta, count(*) from Wlasciciele_firmowych_kont group by Id_konta;

	if exists (select d.Id_konta from @table_d d inner join @table_a a on d.Id_Konta = a.Id_Konta inner join Konta k on k.Id_konta = A.Id_konta where d.count = a.count)
	begin

		RAISERROR('Nie możesz usunąć ostatniego właściela konta', 1, 1)
		return;
	end
	Delete Wlasciciele_firmowych_kont from Wlasciciele_firmowych_kont inner join deleted on Wlasciciele_firmowych_kont.Id_konta = deleted.Id_konta and Wlasciciele_firmowych_kont.Id_firmy = deleted.Id_firmy;
go

Create Trigger Usun_Konto On Konta
Instead of delete
as
	Delete Karty_Konta from Karty_Konta inner join deleted on Karty_Konta.Id_konta = deleted.Id_konta;
	ALTER TABLE Wlasciciele_prywatnych_kont NOCHECK CONSTRAINT ALL
	ALTER TABLE Wlasciciele_firmowych_kont NOCHECK CONSTRAINT ALL
	Delete Konta from Konta inner join deleted on Konta.Id_konta = deleted.Id_konta;
	Delete Wlasciciele_prywatnych_kont from Wlasciciele_prywatnych_kont inner join deleted on Wlasciciele_prywatnych_kont.Id_konta = deleted.Id_konta;
	Delete Wlasciciele_firmowych_kont from Wlasciciele_firmowych_kont inner join deleted on Wlasciciele_firmowych_kont.Id_konta = deleted.Id_konta;
	ALTER TABLE Wlasciciele_prywatnych_kont WITH CHECK CHECK CONSTRAINT ALL
	ALTER TABLE Wlasciciele_firmowych_kont WITH CHECK CHECK CONSTRAINT ALL
go



Create Trigger Usun_Klienta on Klienci
Instead of delete
as
	Declare @table_c table (Id int, count int)
	insert into @table_C select Id_konta, count(*) from Wlasciciele_prywatnych_kont group by Id_konta
	Declare cursor_k CURSOR FOR select Id_klienta from deleted
	for read only
	Declare @Id int
	open cursor_k 
	fetch cursor_k into @id
						
	while @@FETCH_STATUS <> -1
		begin
			Declare @table_k table (Id int)
			insert into @table_k select Id_konta from Wlasciciele_prywatnych_kont where Id_klienta = @id;
			
			Declare cursor_w CURSOR FOR select Id from @table_k
			for read only
			Declare @Id_konta int
			open cursor_W
			fetch cursor_W into @Id_konta
			while @@FETCH_STATUS <> -1
				begin
					if ((select count from @table_C where Id = @Id_konta) = 1)
						begin
							Delete from @table_c where Id = @Id_konta
							Delete from Konta where Id_konta = @Id_konta
						end
				else
					begin
						update @table_c set count = count - 1 where Id = @Id_konta
						Delete FROM Wlasciciele_prywatnych_kont where Id_konta = @Id_konta and Id_klienta = @Id
					end
					fetch cursor_W into @Id_konta
				end
		CLOSE cursor_W
		DEALLOCATE cursor_W
		fetch cursor_k into @id
		end

		CLOSE cursor_k
		DEALLOCATE cursor_k
		Delete Karty from Karty inner join deleted on Karty.Id_klienta = deleted.Id_klienta;
		Delete Klienci from Klienci k inner join deleted d on d.Id_klienta = k.Id_klienta; 
go



--SEKWENCJA DO ID TRANSAKCJI

CREATE SEQUENCE ID_TRANSAKCJI START WITH 1 INCREMENT BY 1
--Wstawianie danych

Insert Into Klienci Values
('Brygida','Paw?owska','11241254466','Kujawska 4','07-393','Bielsko-Bia?a','Polska'),
('Adrian','G?owacki','55122804898','Szkolna 1','88-279','Opole','Polska'),
('Bo?ena','Polak','77031434340','Kujawska 66','63-494','Mys?owice','Polska'),
('Dagmara','Romanowska','85091254107','Le?na 5','57-079','Siemianowice ?l?skie','Polska'),
('Renata','Górska','87101973208','Sienkiewicza 45','48-257','Wa?brzych','Polska'),
('Bogus?aw','Ciesielski','14261618438','??kowa 2','04-318','Rzeszów','Polska'),
('Pelagia','Paj?k','99050276826','Rolna 44','25-851','Grudzi?dz','Polska'),
('Patrycja','Ostrowska','72071813029','Okopowa 17','88-452','Kielce','Polska'),
('Maksym','W?odarczyk','43092809276','Konopnickiej 54','18-600','Bytom','Polska')

Insert into Firmy Values
('Base','6370446984','Dworcowa 2','30-634','W?oc?awek','Polska'),
('Senseship','8110693310','Kasztanowa 21','98-731','Mys?owice','Polska'),
('Ensense','9494807638','?wikli?skiej 44','84-481','W?oc?awek','Polska'),
('Senseatlas','9767305581','Rus?ana 32','32-529','Koszalin','Polska'),
('Senseloop','6486623508','Jutrzenki 2','02-566','Jaworzno','Polska'),
('Jaworzno','3797751916','Le?na 76','20-351','Grudzi?dz','Polska'),
('Appsense','6319409969','?wikli?skiej 43','52-640','Tarnobrzeg','Polska'),
('Fairsense','1271130175','Sosnowa 31','09-719','Jastrz?bie-Zdrój','Polska')

Insert into Bankomaty values
(1,'Ludowa 33','31-081','Wa?brzych','Polska'),
(1,'Struga 77','64-191','Chorzów','Polska'),
(0,'Janaszka 65','36-786','Piekary ?l?skie','Polska'),
(0,'Go?u?skiego 86','17-848','Kielce','Polska'),
(1,'Skromna 87','92-190','Tarnobrzeg','Polska'),
(0,'Freta 8','25-249','Bia?a Podlaska','Polska'),
(1,'Lipowa 99','67-781','?ory','Polska'),
(0,'Z?bkowicka 45','40-222','Lublin','Polska')

Insert into Rodzaje_kont Values
('Osobiste'),
('Firmowe'),
('Oszcz?dno?ciowe'),
('Walutowe'),
('M?odzie?owe'),
('Studenckie')



Insert Into Waluty Values
('Frank szwajcarski','CHF'),
('Polski z?oty','PLN'),
('Euro','EUR'),
('Funt brytyjski','GBP'),
('Dolar ameryka?ski','USD'),
('Dirham Zjednoczonych Emiratów Arabskich','AED'),
('Dolar australijski','AUD'),
('Lew bu?garski','BGN'),
('Dolar kanadyjski','CAD'),
('Juan chi?ski','CNY'),
('Korona czeska','CZK'),
('Korona du?ska','DKK'),
('Dolar hongko?ski','HKD'),
('Kuna chorwacka','HRK'),
('Nowy szekel izraelski','ILS'),
('Jen japo?ski','JPY'),
('Peso meksyka?skie','MXN'),
('Korona norweska','NOK'),
('Dolar nowozelandzki','NZD'),
('Lej rumu?ski','RON')



exec zalorz_konto_prywatne @Nr_Konta = '94521887779731166', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 1;
exec zalorz_konto_prywatne @Nr_Konta = '74253076465253429', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 1;
exec zalorz_konto_prywatne @Nr_Konta = '76284142593440341', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 1;
exec zalorz_konto_prywatne @Nr_Konta = '43829684281789497', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 1;
exec zalorz_konto_prywatne @Nr_Konta = '78660560649254462', @Id_typu = 1, @Kraj = 'Niemcy', @Waluta = 2, @Wlasciel = 1;
exec zalorz_konto_prywatne @Nr_Konta = '65545861579879960', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 1;

Insert into Typy_kart Values
('Kredytowa'),('Debetowa'),('Obci??eniowa')

SET IDENTITY_INSERT Karty ON

Insert into Karty(Nr_karty, Id_klienta, Id_typu_karty) Values
(1,1,1),
(2,2,2),
(3,3,3),
(4,3,2),
(5,1,2),
(6,2,3)

SET IDENTITY_INSERT Karty off

select * from Konta;
select * from Wlasciciele_prywatnych_kont
select * from Karty;

