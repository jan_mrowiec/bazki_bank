--SEKWENCJA DO ID TRANSAKCJI

USE System_Bankowy;

Insert Into Klienci Values
('Brygida','Paw?owska','11241254466','Kujawska 4','07-393','Bielsko-Bia?a','Polska'),
('Adrian','G?owacki','55122804898','Szkolna 1','88-279','Opole','Polska'),
('Bo?ena','Polak','77031434340','Kujawska 66','63-494','Mys?owice','Polska'),
('Dagmara','Romanowska','85091254107','Le?na 5','57-079','Siemianowice ?l?skie','Polska'),
('Renata','Górska','87101973208','Sienkiewicza 45','48-257','Wa?brzych','Polska'),
('Bogus?aw','Ciesielski','14261618438','??kowa 2','04-318','Rzeszów','Polska'),
('Pelagia','Paj?k','99050276826','Rolna 44','25-851','Grudzi?dz','Polska'),
('Patrycja','Ostrowska','72071813029','Okopowa 17','88-452','Kielce','Polska'),
('Maksym','W?odarczyk','43092809276','Konopnickiej 54','18-600','Bytom','Polska'),
('Nurik','Samerkajew','48292809276','Wolna 24','18-600','Nur-Sultan','Kazachastan'),
('Jan','Parowiec','55525035345','Gorska 26','43-300','Wilkowice','Polska'),
('Piotr','Lobsiewicz','46146132778','Wloska 26','54-300','Bielsko-Biala','Polska'),
('Pawel','Przecinnik','19550323201','Morksa 83','40-300','Zabrze','Polska'),
('Adam','Pawlowski','59883425728','Orzechowa 2','40-300','Zabrze','Polska'),
('Wojtek','Bakdorowski','75732885476','Szczesc Boze 1','40-300','Zabrze','Polska'),
('Radek','Tlusty','15716006000','Markana 22','40-300','Zabrze','Polska'),
('Tomasz','Kominiarz','98024024594','Kazimerza 3','43-300','Bielsko-Biala','Polska'),
('Bartosz','Koleda','26645804334','Bochaterow  23','20-300','Mielec','Polska'),
('Daniel','Bogusz','93040283812','Zlotych Lan 87','43-300','Bielsko-Biala','Polska')

Insert into Firmy Values
('Base','6370446984','Dworcowa 2','30-634','W?oc?awek','Polska'),
('Senseship','8110693310','Kasztanowa 21','98-731','Mys?owice','Polska'),
('Ensense','9494807638','?wikli?skiej 44','84-481','W?oc?awek','Polska'),
('Senseatlas','9767305581','Rus?ana 32','32-529','Koszalin','Polska'),
('Senseloop','6486623508','Jutrzenki 2','02-566','Jaworzno','Polska'),
('Jaworzno','3797751916','Le?na 76','20-351','Grudzi?dz','Polska'),
('Appsense','6319409969','?wikli?skiej 43','52-640','Tarnobrzeg','Polska'),
('Fairsense','1271130175','Sosnowa 31','09-719','Jastrz?bie-Zdrój','Polska')

Insert into Bankomaty values
(1,'Ludowa 33','31-081','Wa?brzych','Polska'),
(1,'Struga 77','64-191','Chorzów','Polska'),
(0,'Janaszka 65','36-786','Piekary ?l?skie','Polska'),
(0,'Go?u?skiego 86','17-848','Kielce','Polska'),
(1,'Skromna 87','92-190','Tarnobrzeg','Polska'),
(0,'Freta 8','25-249','Bia?a Podlaska','Polska'),
(1,'Lipowa 99','67-781','?ory','Polska'),
(0,'Z?bkowicka 45','40-222','Lublin','Polska')

Insert into Rodzaje_kont Values
('Osobiste'),
('Firmowe'),
('Oszcz?dno?ciowe'),
('Walutowe'),
('M?odzie?owe'),
('Studenckie')



Insert Into Waluty Values
('Frank szwajcarski','CHF', 4.45),
('Polski z?oty','PLN', 1),
('Euro','EUR', 4.59),
('Funt brytyjski','GBP', 5.54),
('Dolar ameryka?ski','USD', 4.11),
('Dirham Zjednoczonych Emiratów Arabskich','AED', 3.3),
('Dolar australijski','AUD', 2.90),
('Lew bu?garski','BGN', 2.32),
('Dolar kanadyjski','CAD', 3.23),
('Juan chi?ski','CNY', 0.64),
('Korona czeska','CZK', 0.18),
('Korona du?ska','DKK', 0.61),
('Dolar hongko?ski','HKD', 0.52),
('Kuna chorwacka','HRK', 0.61),
('Nowy szekel izraelski','ILS', 1.29),
('Jen japo?ski','JPY', 3.56),
('Peso meksyka?skie','MXN', 0.19),
('Korona norweska','NOK', 0.45),
('Dolar nowozelandzki','NZD', 2.7),
('Lej rumu?ski','RON', 0.92)



exec zalorz_konto_prywatne @Nr_Konta = '94521887779731166', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 3;
exec zalorz_konto_prywatne @Nr_Konta = '74253076465253429', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 2;
exec zalorz_konto_prywatne @Nr_Konta = '76284142593440341', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 3;
exec zalorz_konto_prywatne @Nr_Konta = '43829684281789497', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 4;
exec zalorz_konto_prywatne @Nr_Konta = '78660560649254462', @Id_typu = 1, @Kraj = 'Niemcy', @Waluta = 2, @Wlasciel = 5;
exec zalorz_konto_prywatne @Nr_Konta = '65545861579879960', @Id_typu = 1, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 3;
exec zalorz_konto_prywatne @Nr_Konta = '65545861579829960', @Id_typu = 4, @Kraj = 'Polska', @Waluta = 1, @Wlasciel = 2;

exec zalorz_konto_prywatne @Nr_Konta = '44203752785691165', @Id_typu = 6, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 13;
exec zalorz_konto_prywatne @Nr_Konta = '94580299159222775', @Id_typu = 6, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 14;
exec zalorz_konto_prywatne @Nr_Konta = '21165747994518539', @Id_typu = 6, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 15;
exec zalorz_konto_prywatne @Nr_Konta = '90167997973480030', @Id_typu = 6, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 16;
exec zalorz_konto_prywatne @Nr_Konta = '99434122711781091', @Id_typu = 6, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 17;
exec zalorz_konto_prywatne @Nr_Konta = '48890322859699316', @Id_typu = 6, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 18;
exec zalorz_konto_prywatne @Nr_Konta = '63407237840664100', @Id_typu = 6, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 19;

exec zalorz_konto_prywatne @Nr_Konta = '96060761450734350', @Id_typu = 5, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 6;
exec zalorz_konto_prywatne @Nr_Konta = '93795134751956663', @Id_typu = 5, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 7;
exec zalorz_konto_prywatne @Nr_Konta = '68067545544288986', @Id_typu = 5, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 8;
exec zalorz_konto_prywatne @Nr_Konta = '62836358335994314', @Id_typu = 5, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 9;
exec zalorz_konto_prywatne @Nr_Konta = '09519335731118882', @Id_typu = 5, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 10;
exec zalorz_konto_prywatne @Nr_Konta = '79282406896158454', @Id_typu = 5, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 11;
exec zalorz_konto_prywatne @Nr_Konta = '06255038852929345', @Id_typu = 5, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 12;

exec zalorz_konto_prywatne @Nr_Konta = '63863504130831705', @Id_typu = 4, @Kraj = 'Ukraina', @Waluta = 11, @Wlasciel = 9;
exec zalorz_konto_prywatne @Nr_Konta = '55611808244484579', @Id_typu = 4, @Kraj = 'Bialorus', @Waluta = 10, @Wlasciel = 10;
exec zalorz_konto_prywatne @Nr_Konta = '69022191637235944', @Id_typu = 4, @Kraj = 'Kazachstan', @Waluta = 8, @Wlasciel = 11;
exec zalorz_konto_prywatne @Nr_Konta = '58769402309058648', @Id_typu = 4, @Kraj = 'Niemcy', @Waluta = 7, @Wlasciel = 12;
exec zalorz_konto_prywatne @Nr_Konta = '81874052012963533', @Id_typu = 4, @Kraj = 'Hiszpania', @Waluta = 6, @Wlasciel = 14;
exec zalorz_konto_prywatne @Nr_Konta = '41686714404007495', @Id_typu = 4, @Kraj = 'UK', @Waluta = 3, @Wlasciel = 15;
exec zalorz_konto_prywatne @Nr_Konta = '79100171405376774', @Id_typu = 4, @Kraj = 'USA', @Waluta = 4, @Wlasciel = 16;
exec zalorz_konto_prywatne @Nr_Konta = '74857194709614075', @Id_typu = 4, @Kraj = 'Holandia', @Waluta = 5, @Wlasciel = 17;


exec zalorz_konto_firmowe @Nr_Konta = '10727144594030943', @Id_typu = 2, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 1;
exec zalorz_konto_firmowe @Nr_Konta = '51314859414493424', @Id_typu = 2, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 2;
exec zalorz_konto_firmowe @Nr_Konta = '58645597605538051', @Id_typu = 2, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 3;
exec zalorz_konto_firmowe @Nr_Konta = '41367534945924268', @Id_typu = 2, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 4;
exec zalorz_konto_firmowe @Nr_Konta = '85381913415219992', @Id_typu = 2, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 5;
exec zalorz_konto_firmowe @Nr_Konta = '27378384786622231', @Id_typu = 2, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 6;
exec zalorz_konto_firmowe @Nr_Konta = '97108765038163779', @Id_typu = 2, @Kraj = 'Polska', @Waluta = 2, @Wlasciel = 7;


Insert into Typy_kart Values
('Kredytowa'),('Debetowa'),('Obci??eniowa')



Insert into Karty(Nr_karty, Id_klienta, Id_typu_karty) Values
('4476022257422074',1,1),
('5667405529525136',2,2),
('6203926860428308',3,3),
('6397363068748618',4,2),
('3246166659597237',5,2),
('4300913134242906',6,3),

('3453474016384898',7,1),
('2970082450618476',8,2),
('8451486864434221',9,3),
('2908731989084313',10,2),
('1328167402776318',11,2),
('5480424523435806',12,3),

('3217690985348660',13,1),
('0680572060253417',14,2),
('6954114256986087',15,3),
('7403677714084580',16,2),
('8618983446853340',17,2),
('1323497464039341',18,3),

('6738525460872740',10,2),
('7123811405183169',11,2),
('9403348519918922',12,3),
('9571376526912955',14,2),
('6075202145687700',15,3),
('5873548340083317',16,2)


insert into Wlasciciele_prywatnych_kont values
(1,1),
(1,2),
(1,3)


Insert into Karty_Konta values
(1,1),
(2,2),
(1,3),
(4,4),
(6,15),
(7,16),
(10, 23),
(11, 24),
(12, 21),
(13,8),
(15, 27),
(16,11)


Insert into Pracownicy_firm values
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 6),
(2, 7),
(2, 1),
(2, 2),
(3, 10),
(3, 11),
(3, 12),
(3, 13),
(4, 2),
(4, 5),
(4, 10),
(4, 11),
(4, 15)

exec Wplata @NrKarty = 1,@IdKonta = 1, @NrBankomatu = 1, @Kwota  = 20
exec Wplata @NrKarty = 2,@IdKonta = 2, @NrBankomatu = 1, @Kwota  = 600
exec Wplata @NrKarty = 1,@IdKonta = 3, @NrBankomatu = 1, @Kwota  = 720
exec Wplata @NrKarty = 4,@IdKonta = 4, @NrBankomatu = 1, @Kwota  = 740
exec Wplata @NrKarty = 13,@IdKonta = 8, @NrBankomatu = 1, @Kwota  = 750
exec Wplata @NrKarty = 16,@IdKonta = 11, @NrBankomatu = 1, @Kwota  = 21
exec Wplata @NrKarty = 6,@IdKonta = 15, @NrBankomatu = 1, @Kwota  = 1024
exec Wplata @NrKarty = 7, @IdKonta = 16, @NrBankomatu = 1, @Kwota  = 2137
exec Wplata @NrKarty = 12,@IdKonta = 21, @NrBankomatu = 1, @Kwota  = 420
exec Wplata @NrKarty = 10,@IdKonta = 23, @NrBankomatu = 1, @Kwota  = 0.2
exec Wplata @NrKarty = 11,@IdKonta = 24, @NrBankomatu = 1, @Kwota  = 666
exec Wplata @NrKarty = 15,@IdKonta = 27, @NrBankomatu = 1, @Kwota  = 2313

exec Wyplata @NrKarty = 1, @Id_konta = 1, @NrBankomatu =1 , @Kwota =20
exec Wyplata @NrKarty = 1, @Id_konta = 2, @NrBankomatu =2 , @Kwota =2
exec Wyplata @NrKarty = 2, @Id_konta = 3, @NrBankomatu =3 , @Kwota =20
exec Wyplata @NrKarty = 3, @Id_konta = 4, @NrBankomatu =4 , @Kwota =20
exec Wyplata @NrKarty = 4, @Id_konta = 4, @NrBankomatu =5 , @Kwota =20
exec Wyplata @NrKarty = 5, @Id_konta = 6, @NrBankomatu =6 , @Kwota =20
exec Wyplata @NrKarty = 6, @Id_konta = 7, @NrBankomatu =7 , @Kwota =20
exec Wyplata @NrKarty = 7, @Id_konta = 8, @NrBankomatu =8 , @Kwota =20
exec Wyplata @NrKarty = 8, @Id_konta = 9, @NrBankomatu =9 , @Kwota =20

exec PrzelewNatychmiastowy @IdNadawcy =1, @IdOdbiorcy =2, @Kwota = 10, @Tytul = 'aparat fotograficzny'
exec PrzelewNatychmiastowy @IdNadawcy =2, @IdOdbiorcy =1000, @Kwota = 110, @Tytul = 'brat'   
exec PrzelewNatychmiastowy @IdNadawcy =2, @IdOdbiorcy =3, @Kwota = -10, @Tytul = 'rak'  
exec PrzelewNatychmiastowy @IdNadawcy =1, @IdOdbiorcy =4, @Kwota = 1110, @Tytul = 'agent'    
exec PrzelewNatychmiastowy @IdNadawcy =4, @IdOdbiorcy =3, @Kwota = 111110, @Tytul = 'aparat fotograficzny'
exec PrzelewNatychmiastowy @IdNadawcy =5, @IdOdbiorcy =4, @Kwota = 110, @Tytul = 'brat'   
exec PrzelewNatychmiastowy @IdNadawcy =6, @IdOdbiorcy =6, @Kwota = -10, @Tytul = 'rak'  
exec PrzelewNatychmiastowy @IdNadawcy =7, @IdOdbiorcy =1, @Kwota = 110, @Tytul = 'agent'    

exec PlatnoscKarta @Id_karty = 1, @Id_konta = 2, @Kwota = 100, @Tytul ='Płace za zakupy'
exec PlatnoscKarta @Id_karty = 1, @Id_konta = 1, @Kwota = -100, @Tytul ='Płace za miesko'
exec PlatnoscKarta @Id_karty = 2, @Id_konta = 2, @Kwota = 10, @Tytul ='Płace za zadnie BD'
exec PlatnoscKarta @Id_karty = 3, @Id_konta = 4, @Kwota = 1000, @Tytul ='Płace za Zadanie ASD'
exec PlatnoscKarta @Id_karty = 4, @Id_konta = 5, @Kwota = 1000, @Tytul ='koniec'
exec PlatnoscKarta @Id_karty = 5, @Id_konta = 6, @Kwota = 100, @Tytul ='rzecz'
exec PlatnoscKarta @Id_karty = 6, @Id_konta = 2, @Kwota = 10, @Tytul ='szybki'
exec PlatnoscKarta @Id_karty = 8, @Id_konta = 1, @Kwota = 10, @Tytul ='poważny'
exec PlatnoscKarta @Id_karty = 9, @Id_konta = 3, @Kwota = 10, @Tytul ='Płace za zakupy'
exec PlatnoscKarta @Id_karty = 1, @Id_konta = 4, @Kwota = 10, @Tytul ='Płace za zakupy'
exec PlatnoscKarta @Id_karty = 2, @Id_konta = 5, @Kwota = 10, @Tytul ='Płace za zakupy'
exec PlatnoscKarta @Id_karty = 3, @Id_konta = 6, @Kwota = 10, @Tytul ='Płace za zakupy'