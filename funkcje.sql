--DODATKOWE FUNKCJE

--1. Saldo dla idRachunku

USE System_Bankowy;
go
CREATE FUNCTION SaldoDlaRachunku(@IdRachunku INT)
RETURNS TABLE AS
RETURN (SELECT K.Id_konta, K.Nr_konta, K.Saldo FROM Konta AS K WHERE K.Id_konta = @IdRachunku)

go

Create FUNCTION konto_exists(@Nr_konta nvarchar(28))
RETURNS bit AS
BEGIN
IF exists( select Konta.Nr_konta from Konta where Konta.Nr_konta = @Nr_konta)
	begin
		return 1
	end
return 0 
END

go

CREATE FUNCTION Wlasciciele(@Nr_konta NVARCHAR(28))
RETURNS TABLE AS
	RETURN (select Klienci.* from Wlasciciele_prywatnych_kont inner join Klienci on Wlasciciele_prywatnych_kont.Id_Klienta = Klienci.Id_Klienta inner join Konta on Wlasciciele_prywatnych_kont.Id_konta = Konta.Id_konta where Konta.Nr_konta = @Nr_konta)
go

Create Function Get_Id_Konta(@Nr_konta NVARCHAR(28))
RETURNS INT AS
begin
	RETURN (select Id_konta from Konta where Nr_konta =  @Nr_konta)
end
GO

Create Function Get_Nr_Konta(@Id_konta int)
RETURNS NVARCHAR(28) AS
begin
	RETURN (select Nr_konta from Konta where Id_konta =  @Id_konta)
end
GO

Create Function Get_Id_Karty(@Nr_karty NVARCHAR(16))
RETURNS INT AS
begin
	RETURN (select Id_karty from Karty where Nr_karty =  @Nr_karty)
end
GO

Create Function Get_Nr_Karty(@Id_karty int)
RETURNS NVARCHAR(16) AS
begin
	RETURN (select Nr_karty from Karty where Id_karty =  @Id_karty)
end
GO

Create Function Przelewy_w_okresie(@Od Date, @Do Date)
Returns Table as
	return (select * from Przelewy where Data_realizacji BETWEEN @Od AND @Do)
go

Create Function Karty_dla_Konta(@Id_Konta int)
Returns Table as
	return (select Karty.* from Karty join Karty_Konta on Karty.Id_karty = Karty_Konta.Id_karty where Karty_Konta.Id_konta = @Id_Konta)
go

Create Function Konta_Klienta(@Id_klienta int)
Returns Table as
	return (select Konta.* from Konta join Wlasciciele_prywatnych_kont on Konta.Id_konta = Wlasciciele_prywatnych_kont.Id_konta where Wlasciciele_prywatnych_kont.Id_klienta = @Id_klienta)
go

Create Function Karty_Klienta (@Id_Klienta int)
	Returns Table as
		return (select * from Karty where @Id_Klienta = Id_klienta) 
go

Create Function Dane_Klienta(@Imie nvarchar(20), @Nazwisko nvarchar(20))
	Returns Table as
		return (select * from Klienci where Nazwisko = @Nazwisko and Imie = @Imie)
go
Create Function Pracownicy_firmy(@Id_firmy int)
Returns Table as
	return (select Klienci.* from Klienci join Pracownicy_firm on Klienci.Id_klienta = Pracownicy_firm.Id_klienta where Id_firmy = @Id_firmy)
go

Create Function Klienci_lokalizacja(@Kod nvarchar(10), @Kraj nvarchar(60))
Returns Table as
	return (select * from Klienci where Kraj = @Kraj and Kod_Pocztowy = @Kod)
go

Create Function Firmy_lokalizacja(@Kod nvarchar(10), @Kraj nvarchar(60))
Returns Table as
	return (select * from Firmy where Kraj = @Kraj and Kod_Pocztowy = @Kod)
go

Create Function Klienci_z_local_firmy(@Id_firmy int)
Returns Table as
	return (select K.* from Klienci K inner join (select Kod_Pocztowy, Kraj from Firmy where @Id_firmy = Id_firmy) as T on T.Kraj = k.Kraj AND T.Kod_Pocztowy = K.Kod_Pocztowy)
go