go
create PROCEDURE dbo.Wplata(@NrKarty int, @IdKonta int, @NrBankomatu int, @Kwota money) 
as
Begin transaction
IF(@Kwota <=0) 
BEGIN
	Insert into Anulowane_wplaty Values 
	(@NrBankomatu,@NrKarty, @IdKonta,GetDate(),'Wp?acana kwota musi by? wi?ksza od 0',@Kwota)
	RAISERROR( 'Wp?acana kwota musi by? wi?ksza od 0', 1, 1)
	COMMIT
	RETURN
END
IF not exists (Select * FROM Bankomaty AS B WHERE B.Id_Bankomatu=@NrBankomatu)
BEGIN
	Insert into Anulowane_wplaty Values 
	(@NrBankomatu,@NrKarty, @IdKonta,GetDate(),'Brak bankomatu o takim numerze',@Kwota)
    RAISERROR( 'Brak bankomatu o takim numerze', 1, 1)
	COMMIT
	RETURN
END
IF not exists (Select * FROM Karty AS K WHERE K.Id_karty=@NrKarty)
BEGIN
	Insert into Anulowane_wplaty Values 
	(@NrBankomatu,@NrKarty, @IdKonta,GetDate(),'Brak karty o podanym numerze',@Kwota)
    RAISERROR( 'Brak karty o podanym numerze', 1, 1)
	COMMIT
	RETURN
END
IF not exists (Select * FROM Karty_Konta AS K WHERE K.Id_karty=@NrKarty AND K.Id_konta= @IdKonta)
BEGIN
	Insert into Anulowane_wplaty Values 
	(@NrBankomatu,@NrKarty, @IdKonta,GetDate(),'Dane konto nie istnieje albo nie jest przypisane do danej kary',@Kwota)
    RAISERROR( 'Dane konto nie istnieje albo nie jest przypisane do danej kary', 1, 1)
	COMMIT
	RETURN
END
DECLARE @IdTransakcji int
DECLARE @IdKarty int
SET @IdTransakcji = NEXT VALUE FOR ID_TRANSAKCJI
INSERT INTO Transakcje VALUES (@IdTransakcji,@Kwota)
INSERT INTO Wplaty VALUES (@IdTransakcji,@NrBankomatu, @NrKarty, @IdKonta,GETDATE())
COMMIT
GO






--2. Wyp?ata z konta
create PROCEDURE dbo.Wyplata(@NrKarty int, @Id_konta int, @NrBankomatu int, @Kwota money) 
as
Begin transaction
IF(@Kwota <=0) 
BEGIN
	Insert into Anulowane_wyplaty Values 
	(@NrBankomatu,@NrKarty, @Id_konta,GetDate(),'Wyp?acana kwota musi by? wi?ksza od 0',@Kwota)
    RAISERROR( 'Wyp?acana kwota musi by? wi?ksza od 0', 1 ,1)
	COMMIT
    Return
END
IF @Kwota > (SELECT KO.Saldo FROM Konta AS KO where @Id_konta = KO.Id_konta)
BEGIN
	Insert into Anulowane_wyplaty Values 
	(@NrBankomatu,@NrKarty, @Id_konta,GetDate(),'Niewystarczaj?ca ilo?? ?rodkó na koncie aby dokona? wyp?aty',@Kwota)
    RAISERROR( 'Niewystarczaj?ca ilo?? ?rodkó na koncie aby dokona? wyp?aty', 1, 1 )
	COMMIT
    Return
END
IF not exists (Select * FROM Bankomaty AS B WHERE B.Id_Bankomatu=@NrBankomatu)
BEGIN
	Insert into Anulowane_wyplaty Values 
	(@NrBankomatu,@NrKarty, @Id_konta,GetDate(),'Brak bankomatu o takim numerze',@Kwota)
    RAISERROR( 'Brak bankomatu o takim numerze', 1, 1)
	COMMIT
    Return
END
IF 0 = (Select B.Wplatomat FROM Bankomaty AS B WHERE B.Id_Bankomatu=@NrBankomatu)
BEGIN
	Insert into Anulowane_wyplaty Values 
	(@NrBankomatu,@NrKarty, @Id_konta,GetDate(),'Podany bankomat nie jest wp?atomatem',@Kwota)
    RAISERROR( 'Podany bankomat nie jest wp?atomatem', 1, 1)
	COMMIT
    Return
END
IF not exists (Select * FROM Karty AS K WHERE K.Id_karty=@NrKarty)
BEGIN
	Insert into Anulowane_wyplaty Values 
	(@NrBankomatu,@NrKarty, @Id_konta,GetDate(), 'Brak konta o takim numerze karty',@Kwota)
    RAISERROR( 'Brak konta o takim numerze karty', 1, 1)
	COMMIT
    Return
END

IF not exists (Select * FROM Karty_Konta AS K WHERE K.Id_karty=@NrKarty AND K.Id_konta= @Id_konta)
BEGIN
	Insert into Anulowane_wyplaty Values 
	(@NrBankomatu,@NrKarty, @Id_konta,GetDate(), 'Dane konto nie istnieje albo nie jest przypisane do danej kary',@Kwota)
    RAISERROR( 'Dane konto nie istnieje albo nie jest przypisane do danej kary', 1, 1)
	COMMIT
	RETURN
END
DECLARE @IdTransakcji int
SET @IdTransakcji = NEXT VALUE FOR ID_TRANSAKCJI
INSERT INTO Transakcje VALUES (@IdTransakcji,@Kwota)
INSERT INTO Wplaty VALUES (@IdTransakcji,@NrBankomatu,@NrKarty, @Id_konta,GETDATE())
COMMIT
GO


--3. Natychmiastowy przelew
create PROC dbo.PrzelewNatychmiastowy(
    @IdNadawcy int,
    @IdOdbiorcy int, 
    @Kwota Money, 
    @Tytul NVARCHAR(100)
    )
AS
BEGIN TRANSACTION
if not exists (Select K.Id_konta FROM Konta as K WHERE K.Id_konta = @IdNadawcy)
BEGIN
	Insert into Anulowane_przelewy Values(@IdNadawcy,@IdOdbiorcy, GetDate(), GetDate(), @Tytul,'Nie istnieje konto nadawcy',@Kwota)
    RAISERROR( 'Nie istnieje konto nadawcy', 1, 1)
    COMMIT
    return
END
if not exists (Select K.Id_konta FROM Konta as K WHERE K.Id_konta = @IdOdbiorcy)
BEGIN
	Insert into Anulowane_przelewy Values(@IdNadawcy,@IdOdbiorcy, GetDate(), GetDate(), @Tytul,'Nie istnieje konto odbiorcy',@Kwota)
    RAISERROR( 'Nie istnieje konto odbiorcy', 1, 1)
    COMMIT
    return
END
if @Kwota < 0
BEGIN
	Insert into Anulowane_przelewy Values(@IdNadawcy,@IdOdbiorcy, GetDate(), GetDate(), @Tytul,'Kwota musi by? wi?ksza od 0',@Kwota)
    RAISERROR( 'Kwota musi by? wi?ksza od 0', 1, 1)
    COMMIT
    return
END
if @Kwota > (Select K.Saldo FROM Konta as K Where K.Id_konta = @IdNadawcy)
BEGIN
	Insert into Anulowane_przelewy Values(@IdNadawcy,@IdOdbiorcy, GetDate(), GetDate(), @Tytul,'Niewystarczaj?ca kwota na koncie nadawcy',@Kwota)
    RAISERROR( 'Niewystarczaj?ca kwota na koncie nadawcy', 1, 1)
    COMMIT
    return
END
UPDATE Konta 
SET Saldo = Saldo - @Kwota
Where Id_konta = @IdNadawcy

UPDATE Konta 
SET Saldo = Saldo + @Kwota
Where Id_konta = @IdOdbiorcy

Declare @IdTransakcji INT
SET @IdTransakcji = (NEXT VALUE FOR ID_TRANSAKCJI)

INSERT INTO Transakcje Values 
(@IdTransakcji, @Kwota)

INSERT INTO Przelewy Values
(@IdTransakcji,@IdNadawcy,@IdOdbiorcy,'',GETDATE(), GETDATE(),@Tytul)

IF @@ERROR = 0
BEGIN
    PRINT 'Transakcja zako?czy?a si? powodzeniem'
    COMMIT TRANSACTION
END
ELSE
BEGIN
    RAISERROR('Nast?pi? b??d',16,34)
    ROLLBACK TRANSACTION
END
GO


Create PROCEDURE zalorz_konto_prywatne (@Nr_konta NVARCHAR(28), @Id_typu int, @Kraj nvarchar(10), @Waluta int, @Wlasciel int)
AS
BEGIN TRANSACTION
IF (dbo.konto_exists(@Nr_konta) = 1)
Begin
	RAISERROR( 'To konto juz istnieje', 1, 1);
	ROLLBACK
	return
END

IF not exists ( select Klienci.Id_Klienta from Klienci where Klienci.Id_Klienta = @Wlasciel)
Begin
	RAISERROR( 'Podany klient nie istnieje', 1, 1);
	ROLLBACK
	return
END
	INSERT INTO Konta(Id_typu_konta, Kraj, Nr_konta, Saldo, Id_waluty) values (@Id_typu, @Kraj, @Nr_konta, 0, @Waluta);
	declare @Id_konta int = (select Id_konta from Konta K where K.Nr_konta = @Nr_konta)
	INSERT INTO Wlasciciele_prywatnych_kont values (@Wlasciel,@Id_konta);
	COMMIT
GO


Create PROCEDURE zalorz_konto_firmowe (@Nr_konta NVARCHAR(28), @Kraj nvarchar(10), @Waluta int, @Wlasciel int)
AS
BEGIN TRANSACTION
IF (dbo.konto_exists(@Nr_konta) = 1)
Begin
	RAISERROR( 'To konto juz istnieje', 1, 1);
	ROLLBACK
	return
END

IF not exists ( select Firmy.Id_firmy from Firmy where Id_firmy = @Wlasciel)
Begin
	RAISERROR( 'Podana firma nie istnieje', 1, 1);
	ROLLBACK
	return
END
	INSERT INTO Konta(Id_typu_konta, Kraj, Nr_konta, Saldo, Id_waluty) values (2, @Kraj, @Nr_konta, 0, @Waluta);
	declare @Id_konta int = (select Id_konta from Konta K where K.Nr_konta = @Nr_konta)
	INSERT INTO Wlasciciele_firmowych_kont values (@Wlasciel, @Id_konta);
	COMMIT
GO


--4. P?atno?? Kart?
GO
--4. P?atno?? Kart?
Create PROC PlatnoscKarta (@Id_karty int, @Id_konta int, @Kwota money, @Tytul NVARCHAR(100))
AS
BEGIN TRANSACTION
if @Kwota <= 0
BEGIN
	INSERT INTO Anulowane_platnosci Values(@Id_karty , @Id_konta,GETDATE(),@Tytul,'Kwota musi byc wieksza od 0',@Kwota)
    RAISERROR( 'Kwota musi byc wieksza od 0', 1, 1)
    COMMIT
    RETURN
END
IF not exists (Select * FROM Karty_Konta AS K WHERE K.Id_karty=@Id_karty and K.Id_konta = @Id_konta)
BEGIN
	INSERT INTO Anulowane_platnosci Values(@Id_karty , @Id_konta,GETDATE(),@Tytul,'Brak konta o takim numerze karty',@Kwota)
    RAISERROR( 'Brak konta o takim numerze karty', 1, 1)
    COMMIT
    Return
END
IF @Kwota > (
SELECT K.Saldo 
FROM Konta AS K 
WHERE K.Id_konta=@Id_konta) 
BEGIN
	INSERT INTO Anulowane_platnosci Values(@Id_karty , @Id_konta,GETDATE(),@Tytul,'Nie wystarcajaca ilosc ?rodków na koncie',@Kwota)
    RAISERROR( 'Nie wystarcajaca ilosc ?rodków na koncie', 1, 1)
    COMMIT
    Return
END
DECLARE @IdTransakcji INT
SET @IdTransakcji = (NEXT VALUE FOR ID_TRANSAKCJI)

INSERT INTO Transakcje Values 
(@IdTransakcji, @Kwota)

INSERT INTO Platnosci Values
(@IdTransakcji,@Id_karty, @Id_konta, GETDATE(),@Tytul)

UPDATE Konta
SET Saldo = Saldo - @Kwota
WHERE Id_konta = @Id_konta

IF @@ERROR = 0
BEGIN
    PRINT 'Transakcja zako?czy?a si? powodzeniem'
    COMMIT TRANSACTION
END
ELSE
BEGIN
    RAISERROR('Nast?pi? b??d',16,34)
    ROLLBACK TRANSACTION
END
go

Create Procedure Przelew_walutowy(@Id_nadawcy int, @Id_odbiorcy int, @Kwota money, @Tytul NVARCHAR(100)) as
BEGIN TRANSACTION
if not exists ( select Id_konta from Konta where Id_konta = @Id_nadawcy)
BEGIN
    RAISERROR('Nie istnieje kotno nadawcy',16,34)
    ROLLBACK TRANSACTION
	return
END
if not exists ( select Id_konta from Konta where Id_konta = @Id_odbiorcy)
BEGIN
    RAISERROR('Nie istnieje kotno odbiorcy',16,34)
    ROLLBACK TRANSACTION
	return
END
if @Kwota > (select Saldo from Konta where Id_konta = @Id_nadawcy)
begin
    RAISERROR('Nie wystarczajaca ilosc srodkow na koncie',16,34)
    ROLLBACK TRANSACTION
	return
end

Declare @kurs_nadawcy int = (select w.Kurs from Konta k inner join Waluty w on k.Id_waluty = w.Id_waluty  where Id_konta = @Id_nadawcy); 
Declare @kurs_odbiorcy int = (select w.Kurs from Konta k inner join Waluty w on k.Id_waluty = w.Id_waluty  where Id_konta = @Id_odbiorcy);  

DECLARE @n_kwota money =  (@Kwota * @kurs_nadawcy) / @kurs_odbiorcy;

Update Konta set Saldo = Saldo - @Kwota where Id_konta = @Id_nadawcy;
Update Konta set Saldo = Saldo + @n_kwota where Id_konta = @Id_odbiorcy;

Declare @IdTransakcji INT
SET @IdTransakcji = (NEXT VALUE FOR ID_TRANSAKCJI)


INSERT INTO Transakcje Values 
(@IdTransakcji, @Kwota)

INSERT INTO Przelewy_walutowe Values
(@IdTransakcji, @Id_nadawcy, @Id_odbiorcy,(select Id_waluty from Konta where Id_konta = @Id_nadawcy), (select Id_waluty from Konta where Id_konta = @Id_odbiorcy), '',GETDATE(), GETDATE(),@Tytul)

IF @@ERROR = 0
BEGIN
    PRINT 'Transakcja zako?czy?a si? powodzeniem'
    COMMIT TRANSACTION
END
ELSE
BEGIN
    RAISERROR('Nast?pi? b??d',16,34)
    ROLLBACK TRANSACTION
end
go


CREATE unique INDEX Pesel_indeks on Klienci(Pesel);
CREATE INDEX Imie_nazwisko_indeks on Klienci(Nazwisko, Imie);
CREATE unique INDEX Konta_indeks on Konta(Nr_Konta);
Create unique INDEX Karty_Indeks on Karty(Nr_Karty);
Create unique Index Firmy_Nip on Firmy(NIP)
Create INDEX Firmy_lokalizacja on Firmy(Kraj, Kod_Pocztowy)
Create INDEX Klienci_lokalizacja on Klienci(Kraj, Kod_Pocztowy)
Create Index Bankomaty_lokalizacja on Bankomaty(Kraj, Kod_Pocztowy)