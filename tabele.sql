Create Database System_Bankowy;

USE System_Bankowy;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET XACT_ABORT on

Create Table Klienci(
    Id_klienta int PRIMARY KEY IDENTITY NOT NULL,
    Imie nvarchar(20) NOT NULL,
    Nazwisko nvarchar(20) NOT NULL,
    Pesel nvarchar(11) UNIQUE NOT NULL,
    Adres nvarchar(80) NOT NULL,
    Kod_Pocztowy nvarchar(10) NOT NULL,
    Miasto nvarchar (60) NOT NULL,
    Kraj nvarchar(60) DEFAULT 'Polska' NOT NULL
)

Create Table Firmy
(
    Id_firmy int PRIMARY KEY IDENTITY NOT NULL,
    Nazwa nvarchar(60) NOT NULL,
    NIP nvarchar(10) UNIQUE NOT NULL,
    Adres nvarchar(80) NOT NULL,
    Kod_Pocztowy nvarchar(10) NOT NULL,
    Miasto nvarchar (60) NOT NULL,
    Kraj nvarchar(60) DEFAULT 'Polska' NOT NULL
)

Create Table Bankomaty
(
    Id_Bankomatu int PRIMARY KEY IDENTITY NOT NULL,
    Wplatomat bit NOT NULL,
    Adres nvarchar(80) NOT NULL,
    Kod_Pocztowy nvarchar(10) NOT NULL,
    Miasto nvarchar (60) NOT NULL,
    Kraj nvarchar(60) DEFAULT 'Polska' NOT NULL,
)

Create Table Waluty(
    Id_waluty int primary key Identity Not null,
    Nazwa nvarchar(60) NOT NULL,
	Akronim nvarchar(3),
	Kurs float
)

Create Table Rodzaje_kont(
    Id_rodzaju_konta int PRIMARY KEY Identity NOT NULL,
    Rodzaj_Konta varchar(40) NOT NULL,
)



Create Table Konta(
    Id_konta int PRIMARY KEY Identity NOT NULL,
    Id_typu_konta int FOREIGN KEY REFERENCES Rodzaje_kont(Id_rodzaju_konta),
    Kraj nVARCHAR(10),
    Nr_konta nVARCHAR(28),
    Saldo Money,
    Id_waluty int FOREIGN KEY REFERENCES Waluty(Id_waluty),
)

Create table Typy_kart(
    Id_typu_karty int PRIMARY KEY Identity NOT NULL,
    Nazwa varchar(40) NOT NULL,
)


Create Table Karty
(
    Id_karty int PRIMARY KEY IDENTITY NOT NULL,
	Nr_karty nvarchar(16),
    Id_klienta int FOREIGN KEY REFERENCES Klienci(Id_klienta) NOT NULL,
    Id_typu_karty int FOREIGN KEY REFERENCES Typy_kart(Id_typu_karty) NOT NULL
)

Create Table Karty_Konta
(
	Id_karty int FOREIGN KEY REFERENCES Karty(Id_karty) on delete cascade NOT NULL,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) on delete cascade NOT NULL,
	CONSTRAINT PK_kont PRIMARY KEY (Id_karty,Id_konta)
)


Create Table Transakcje
(
    Id_transakcji int PRIMARY KEY NOT NULL,
    Kwota Money,
)

Create Table Przelewy
(
    Id_przelewu int Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_nadawcy int FOREIGN KEY REFERENCES Konta(Id_konta),
    Id_odbiorcy int FOREIGN KEY REFERENCES Konta(Id_konta),
    Status_transakcji char,
    Data_zlecenia DATE Not null,
    Data_realizacji DATE Not null,
    Tytul Nvarchar(100) Not null,
)

Create Table Wplaty
(
    Id_wplaty int  Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_Bankomatu int FOREIGN KEY REFERENCES Bankomaty(Id_Bankomatu) Not null,
    Id_karty int FOREIGN KEY REFERENCES Karty(Id_karty) Not null,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) Not null,
    Data_realizacji Date, 
)

Create Table Wyplaty
(
    Id_wyplaty int Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_Bankomatu int FOREIGN KEY REFERENCES Bankomaty(Id_Bankomatu) Not null,
    Id_karty int FOREIGN KEY REFERENCES Karty(Id_karty) Not null,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) Not null,
    Data_realizacji Date,
)

Create Table Platnosci
(
    Id_platnosci int Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_karty int FOREIGN KEY REFERENCES Karty(Id_karty) Not null,
	Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta) Not null,
    Data_realizacji Date,
    Tytul Nvarchar(100),
)

Create Table Pracownicy_firm
(
    Id_firmy int FOREIGN KEY REFERENCES Firmy(Id_firmy),
    Id_klienta int FOREIGN KEY REFERENCES Klienci(Id_klienta)
)
Create Table Wlasciciele_prywatnych_kont(
    Id_klienta int FOREIGN KEY REFERENCES Klienci(Id_klienta),
    Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta),
	CONSTRAINT PK_p PRIMARY KEY (Id_klienta,Id_konta)
)

Create Table Wlasciciele_firmowych_kont(
    Id_firmy int FOREIGN KEY REFERENCES Firmy(Id_firmy),
    Id_konta int FOREIGN KEY REFERENCES Konta(Id_konta),
	CONSTRAINT PK_f PRIMARY KEY (Id_firmy,Id_konta)
)



Create Table Anulowane_wplaty
(
    Id int  Primary Key IDENTITY ,
    Id_Bankomatu int,
    Id_karty int,
	Id_konta int,
    Data_realizacji Date, 
	Blad NVarchar(100),
	Kwota Money,
)

Create Table Anulowane_wyplaty
(
    Id_wyplaty int Primary Key IDENTITY ,
    Id_Bankomatu int,
    Id_karty int,
	Id_konta int,
    Data_realizacji Date,
	Blad NVarchar(100),
	Kwota Money,
)

Create Table Anulowane_przelewy
(
    Id int Primary Key IDENTITY ,
    Id_nadawcy int,
    Id_odbiorcy int,
    Data_zlecenia DATE Not null,
    Data_realizacji DATE Not null,
    Tytul Nvarchar(100) Not null,
	Blad NVarchar(100),
	Kwota Money,
)

Create Table Anulowane_platnosci
(
    Id_platnosci int Primary Key IDENTITY ,
    Id_karty int,
	Id_konta int,
    Data_realizacji Date,
    Tytul Nvarchar(100),
	Blad NVarchar(100),
	Kwota Money,
)
Create Table Przelewy_walutowe
(
    Id_przelewu int Primary Key FOREIGN KEY REFERENCES Transakcje(Id_transakcji) Not null,
    Id_nadawcy int FOREIGN KEY REFERENCES Konta(Id_konta),
    Id_odbiorcy int FOREIGN KEY REFERENCES Konta(Id_konta),
	Waluta_nad int FOREIGN KEY REFERENCES Waluty(Id_waluty),
	Waluta_odb int FOREIGN KEY REFERENCES Waluty(Id_waluty),
    Status_transakcji char,
    Data_zlecenia DATE Not null,
    Data_realizacji DATE Not null,
    Tytul Nvarchar(100) Not null,
)