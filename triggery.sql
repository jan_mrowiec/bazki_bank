--TRIGGERY
--1. Uaktualnienie po wp?acie



USE System_Bankowy;

go
CREATE TRIGGER Uaktualnij_saldo_po_wplacie ON Wplaty
AFTER INSERT
AS
DECLARE @IdTransakcji INT
DECLARE @KwotaTransakcji Money
DECLARE @IdKarty INT
DECLARE @IdKonta Int
SET @IdTransakcji = (SELECT Id_wplaty FROM inserted)
SET @IdKarty= (SELECT Id_karty FROM inserted)
SET @KwotaTransakcji = (Select T.Kwota FROM Transakcje AS T WHERE T.Id_transakcji = @IdTransakcji)
SET @IdKonta = (SELECT Id_konta FROM inserted)
UPDATE Konta 
SET Saldo = Saldo + @KwotaTransakcji
Where Id_konta = @IdKonta
GO

--2. Uaktualnienie po wyp?acie

CREATE TRIGGER Uaktualnij_saldo_po_wyplacie ON Wyplaty
AFTER INSERT
AS
DECLARE @IdTransakcji INT
DECLARE @KwotaTransakcji Money
DECLARE @IdKarty INT
DECLARE @IdKonta Int
SET @IdTransakcji = (SELECT Id_wyplaty FROM inserted)
SET @IdKarty= (SELECT Id_karty FROM inserted)
SET @KwotaTransakcji = (Select T.Kwota FROM Transakcje AS T WHERE T.Id_transakcji = @IdTransakcji)
SET @IdKonta = (SELECT Id_konta FROM inserted)
UPDATE Konta 
SET Saldo = Saldo - @KwotaTransakcji
Where Id_konta = @IdKonta
GO

Create Trigger Usun_Wlasiciela On Wlasciciele_prywatnych_kont
Instead of delete
as
	DECLARE @table_d table(Id_konta int, count int)
	insert into @table_d select Id_konta, count(*) from deleted group by Id_konta;

	DECLARE @table_a table(Id_konta int, count int)
	insert into @table_a select Id_konta, count(*) from Wlasciciele_prywatnych_kont group by Id_konta;

	if exists (select d.Id_konta from @table_d d inner join @table_a a on d.Id_Konta = a.Id_Konta inner join Konta k on k.Id_konta = A.Id_konta where d.count = a.count)
	begin

		RAISERROR('Nie mo?esz usun?? ostatniego w?a?ciela konta', 1, 1)
		return;
	end
	Delete Wlasciciele_prywatnych_kont from Wlasciciele_prywatnych_kont inner join deleted on Wlasciciele_prywatnych_kont.Id_klienta = deleted.Id_klienta and Wlasciciele_prywatnych_kont.Id_konta = deleted.Id_konta;
go


Create Trigger Usun_Wlasiciela_fir On Wlasciciele_firmowych_kont
Instead of delete
as
	DECLARE @table_d table(Id_konta int, count int)
	insert into @table_d select Id_konta, count(*) from deleted group by Id_konta;

	DECLARE @table_a table(Id_konta int, count int)
	insert into @table_a select Id_konta, count(*) from Wlasciciele_firmowych_kont group by Id_konta;

	if exists (select d.Id_konta from @table_d d inner join @table_a a on d.Id_Konta = a.Id_Konta inner join Konta k on k.Id_konta = A.Id_konta where d.count = a.count)
	begin

		RAISERROR('Nie mo?esz usun?? ostatniego w?a?ciela konta', 1, 1)
		return;
	end
	Delete Wlasciciele_firmowych_kont from Wlasciciele_firmowych_kont inner join deleted on Wlasciciele_firmowych_kont.Id_konta = deleted.Id_konta and Wlasciciele_firmowych_kont.Id_firmy = deleted.Id_firmy;
go

Create Trigger Usun_Konto On Konta
Instead of delete
as
	Delete Karty_Konta from Karty_Konta inner join deleted on Karty_Konta.Id_konta = deleted.Id_konta;
	ALTER TABLE Wlasciciele_prywatnych_kont NOCHECK CONSTRAINT ALL
	ALTER TABLE Wlasciciele_firmowych_kont NOCHECK CONSTRAINT ALL
	Delete Konta from Konta inner join deleted on Konta.Id_konta = deleted.Id_konta;
	Delete Wlasciciele_prywatnych_kont from Wlasciciele_prywatnych_kont inner join deleted on Wlasciciele_prywatnych_kont.Id_konta = deleted.Id_konta;
	Delete Wlasciciele_firmowych_kont from Wlasciciele_firmowych_kont inner join deleted on Wlasciciele_firmowych_kont.Id_konta = deleted.Id_konta;
	ALTER TABLE Wlasciciele_prywatnych_kont WITH CHECK CHECK CONSTRAINT ALL
	ALTER TABLE Wlasciciele_firmowych_kont WITH CHECK CHECK CONSTRAINT ALL
go



Create Trigger Usun_Klienta on Klienci
Instead of delete
as
	Declare @table_c table (Id int, count int)
	insert into @table_C select Id_konta, count(*) from Wlasciciele_prywatnych_kont group by Id_konta
	Declare cursor_k CURSOR FOR select Id_klienta from deleted
	for read only
	Declare @Id int
	open cursor_k 
	fetch cursor_k into @id
						
	while @@FETCH_STATUS <> -1
		begin
			Declare @table_k table (Id int)
			insert into @table_k select Id_konta from Wlasciciele_prywatnych_kont where Id_klienta = @id;
			
			Declare cursor_w CURSOR FOR select Id from @table_k
			for read only
			Declare @Id_konta int
			open cursor_W
			fetch cursor_W into @Id_konta
			while @@FETCH_STATUS <> -1
				begin
					if ((select count from @table_C where Id = @Id_konta) = 1)
						begin
							Delete from @table_c where Id = @Id_konta
							Delete from Konta where Id_konta = @Id_konta
						end
				else
					begin
						update @table_c set count = count - 1 where Id = @Id_konta
						Delete FROM Wlasciciele_prywatnych_kont where Id_konta = @Id_konta and Id_klienta = @Id
					end
					fetch cursor_W into @Id_konta
				end
		CLOSE cursor_W
		DEALLOCATE cursor_W
		fetch cursor_k into @id
		end

		CLOSE cursor_k
		DEALLOCATE cursor_k
		Delete Karty from Karty inner join deleted on Karty.Id_klienta = deleted.Id_klienta;
		Delete Klienci from Klienci k inner join deleted d on d.Id_klienta = k.Id_klienta; 
go


Create Trigger Usun_Firme on Firmy
Instead of delete
as
	Declare @table_c table (Id int, count int)
	insert into @table_C select Id_konta, count(*) from Wlasciciele_firmowych_kont group by Id_konta
	Declare cursor_k CURSOR FOR select Id_firmy from deleted
	for read only
	Declare @Id int
	open cursor_k 
	fetch cursor_k into @id
						
	while @@FETCH_STATUS <> -1
		begin
			Declare @table_k table (Id int)
			insert into @table_k select Id_konta from Wlasciciele_firmowych_kont where Id_firmy = @id;
			
			Declare cursor_w CURSOR FOR select Id from @table_k
			for read only
			Declare @Id_konta int
			open cursor_W
			fetch cursor_W into @Id_konta
			while @@FETCH_STATUS <> -1
				begin
					if ((select count from @table_C where Id = @Id_konta) = 1)
						begin
							Delete from @table_c where Id = @Id_konta
							Delete from Konta where Id_konta = @Id_konta
						end
				else
					begin
						update @table_c set count = count - 1 where Id = @Id_konta
						Delete FROM Wlasciciele_firmowych_kont where Id_konta = @Id_konta and Id_firmy = @Id
					end
					fetch cursor_W into @Id_konta
				end
		CLOSE cursor_W
		DEALLOCATE cursor_W
		fetch cursor_k into @id
		end

		CLOSE cursor_k
		DEALLOCATE cursor_k
		Delete Firmy from Firmy k inner join deleted d on d.Id_firmy = k.Id_firmy; 
go

Create Trigger Polacz_karta_klient on Karty_Konta
Instead of Insert
as
	if exists (select Wlasciciele_prywatnych_kont.Id_klienta from inserted inner join Karty on inserted.Id_karty = Karty.Id_karty left join Wlasciciele_prywatnych_kont on Wlasciciele_prywatnych_kont.Id_klienta = Karty.Id_klienta and Wlasciciele_prywatnych_kont.Id_konta = inserted.Id_konta where Wlasciciele_prywatnych_kont.Id_klienta is null)
	begin
		RAISERROR('Podane konto nie jest przypisane do posiadacza kary', 1, 1)
		return;
	end
	Insert into Karty_Konta
	select * from inserted;
go

Create Trigger Wstaw_wl_pr on Wlasciciele_prywatnych_kont
Instead of Insert
as
	if exists (select i.Id_konta from inserted i inner join Konta k on k.Id_konta = i.Id_konta where Id_typu_konta = 2)
	begin
		RAISERROR('Nie mozesz przypisac firmowego konta prywatnemu wlascicielowi', 1, 1)
		return;
	end
	insert into Wlasciciele_prywatnych_kont
	select * from inserted;
go

Create Trigger Wstaw_wl_fir on Wlasciciele_firmowych_kont
Instead of Insert
as
	if exists (select i.Id_konta from inserted i inner join Konta k on k.Id_konta = i.Id_konta where Id_typu_konta <> 2)
	begin
		RAISERROR('Nie mozesz przypisac prywatnego konta firmie', 1, 1)
		return;
	end
	insert into Wlasciciele_firmowych_kont
	select * from inserted;
go


--SEKWENCJA DO ID TRANSAKCJI

CREATE SEQUENCE ID_TRANSAKCJI START WITH 1 INCREMENT BY 1
--Wstawianie danych